
//canvas
var canvas = document.getElementById('canvas');
var context = canvas.getContext('2d');
canvas.width = 700;
canvas.height = 500;
var radius = 10;
var dragging = false;
var uploadImage = document.getElementById('upImage');
    uploadImage.addEventListener('change', upImage);
context.lineWidth = radius*2;
//redo undo
let state = context.getImageData(0, 0, canvas.width, canvas.height);
window.history.pushState(state, null);

//text
var text=0;
function generatetext(){
    text=1;
}


//shape
var rectangle = document.getElementsByClassName('rectangle')[0];
rectangle.addEventListener('click', drawrectangle);
var triangle = document.getElementsByClassName('triangle')[0];
triangle.addEventListener('click', drawtriangle);
var circle = document.getElementsByClassName('circle')[0];
circle.addEventListener('click', drawcircle);
var shape=0;
var x =0;
var y =0;
var draw=0;

var putPoint = function(e){
  if(draw==1){
    return;
  }
  else if(dragging){
    context.lineTo(e.offsetX,e.offsetY);
    context.stroke();
    context.beginPath();
    context.arc(e.offsetX, e.offsetY, radius, 0, Math.PI*2);
    context.fill();
    context.beginPath();
    context.moveTo(e.offsetX ,e.offsetY);
  }
}
var engage = function(e){
    if(text){
        var size = document.getElementById("mynumber").value;
        var family = document.getElementById("mySelect").value;
        context.font=size+"px "+family;
        x=e.offsetX;
        y=e.offsetY;
        context.fillText(document.getElementById("mytext").value, x, y);
        text=0;
        var state = context.getImageData(0, 0, canvas.width, canvas.height);
        window.history.pushState(state, null);
    }
    else if(draw==1){
        context.beginPath();
        x=e.offsetX;
        y=e.offsetY;
    }
    else{
        dragging = true;
        context.beginPath();
    }
}

var disengage = function(e){
  if(draw==1){
      if(shape==1){
        context.fillRect(x,y,e.offsetX-x, e.offsetY-y);
        context.fillStyle=context.strokeStyle;
        context.stroke();
        context.closePath();
      }
      else if(shape==2){
        context.moveTo(x,y);
        context.lineTo(e.offsetX,y);
        context.lineTo((x+e.offsetX)/2,e.offsetY);
        context.lineTo(x,y);
        context.closePath();
        context.fillStyle=context.strokeStyle;
        context.fill();

      }
      else if(shape==3){
        context.arc(e.offsetX, e.offsetY, Math.sqrt((e.offsetX-x)*(e.offsetX-x)+(e.offsetY-y)*(e.offsetY-y)), 0, Math.PI*2);
        context.closePath();
        context.fillStyle=context.strokeStyle;
        context.fill();

      }
  }else{
    dragging = false;
    context.beginPath();
  }

  var state = context.getImageData(0, 0, canvas.width, canvas.height);
  window.history.pushState(state, null);
}


var cleanbutton = document.getElementById('clean');

cleanbutton.addEventListener('click', cleanImage);

function cleanImage(){
    context.clearRect(0, 0, canvas.width, canvas.height);
    var state = context.getImageData(0, 0, canvas.width, canvas.height);
    window.history.pushState(state, null);
}

function upImage(e){
  var reader = new FileReader();
  reader.onload = function(event){
      var img = new Image();
      img.onload = function(){
          img.width = canvas.width;
          img.height = canvas.height;
          context.drawImage(img,0,0,img.width,img.height);
      }
      img.src = event.target.result;
  }
  reader.readAsDataURL(e.target.files[0]);
  var state = context.getImageData(0, 0, canvas.width, canvas.height);
  window.history.pushState(state, null);
}

function drawrectangle(){
    if(shape==1){
        rectangle.className = 'rectangle';
        shape=0;
        draw=0;
        return;
    }
    else if(shape==2){
        triangle.className = 'triangle';
    }
    else if(shape==3){
        circle.className = 'circle';
    }
    rectangle.className += ' click';
    shape=1;
    draw=1;

}
    
function drawtriangle(){
    if(shape==1){
        rectangle.className = 'rectangle';
    }
    else if(shape==2){  
        triangle.className = 'triangle';
        shape=0;
        draw=0
        return;
    }
    else if(shape==3){
        circle.className= 'circle';
    }
    shape=2;
    triangle.className += ' click' ;
    draw=1;
}
function drawcircle(){
    if(shape==1){
        rectangle.className = 'rectangle';
    }
    else if(shape==2){
        triangle.className = 'triangle';
    }
    else if(shape==3){
        circle.className = 'circle';
        shape=0;
        draw=0
        return;
    }
    shape=3;
    circle.className+= ' click';
    draw=1;
}

canvas.addEventListener('mousedown', engage);
canvas.addEventListener('mousemove',putPoint);
canvas.addEventListener('mouseup', disengage);

//redo undo
window.addEventListener('popstate', changeStep, false);

function changeStep(e){
  context.clearRect(0, 0, canvas.width, canvas.height);

  if( e.state ){
    context.putImageData(e.state, 0, 0);
  }
}